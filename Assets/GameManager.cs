using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    [SerializeField]
    StateManager[] gameStates = new StateManager[4];

    public Dictionary<States, Base> states = new Dictionary<States, Base>();

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    void Start()
    {
        states.Add(States.MENU, gameStates[0]);
        states.Add(States.PLAY, gameStates[1]);
        states.Add(States.GAMEOVER, gameStates[2]);
        states.Add(States.WIN, gameStates[3]);

        StateMachine.instance.Change(states[States.MENU]);
    }


    void Update() => StateMachine.instance.OnTick();






}
