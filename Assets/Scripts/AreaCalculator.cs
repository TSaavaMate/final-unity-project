using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaCalculator : ScriptableObject
{
    public static bool IsInside(Vector3 position,GameObject obj)
    {
        Vector3 center = obj.transform.position;

        float radiusX = obj.transform.localScale.x / 2f;

        float radiusY = obj.transform.localScale.y / 2f;


        float distanceX = Mathf.Abs(position.x - center.x);
        float distanceY = Mathf.Abs(position.y - center.y);


        return (distanceX * distanceX) / (radiusX * radiusX) + (distanceY * distanceY) / (radiusY * radiusY) <= 1 && position.y >= center.y;
    }
}
