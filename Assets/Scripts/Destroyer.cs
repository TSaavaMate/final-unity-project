using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Destroyer : MonoBehaviour,Shootable
{
    protected Vector3 direction;

    public System.Action destroyed;

    public GameObject player;
    public GameObject destroyerRef;


    public void SetParams(GameObject player, GameObject destroyer)
    {
        this.player = player;
        this.destroyerRef = destroyer;
    }

    public virtual void Move(GameObject obj, float speed) => obj.transform.position += direction * speed * Time.deltaTime;


    public void SetDirection(Vector3 direction) => this.direction = direction;


    public void OnCollide(GameObject obj)
    {
            obj.gameObject.SetActive(false);
            if (destroyed != null)
            {
                destroyed.Invoke();
            }
        
    }

    public void Shoot()
    {
        GameObject destroyerObj = Instantiate(this.destroyerRef, transform.position, Quaternion.identity);
        Missile missile = destroyerObj.GetComponent<Missile>();


        Vector3 directionToPlayer = (player.transform.position - missile.transform.position).normalized;

        missile.SetDirection(directionToPlayer);

        float angle = Mathf.Atan2(directionToPlayer.y, directionToPlayer.x) * Mathf.Rad2Deg;

        missile.transform.rotation = Quaternion.Euler(0f, 0f, angle - 90f);
    }


}
