using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour, Shootable
{
    public Vector3 direction { get; set; }

    public System.Action destroyed;

    void Start() => direction = new Vector3(0, 1, 0);



    public virtual void Move(GameObject obj, float speed) => obj.transform.position += direction * speed * Time.deltaTime;



    public void OnCollide(GameObject obj)
    {
        if (destroyed != null)
        {
            destroyed.Invoke();
        }
        Destroy(obj);
    }
}
