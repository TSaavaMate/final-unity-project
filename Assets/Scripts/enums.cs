using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Weapons
{
    LASER = 0,
    BULLET=1,
    ROCKET = 2
}
public enum States
{
    MENU,
    PLAY,
    GAMEOVER,
    WIN
}
public enum Character
{
    PLAYER,
    ENEMY
}
public enum Layers
{
    PLANET=3,
    BOUNDARY=6,
    PLAYER=7,
    PLAYERWEAPON=8,
    WALL=9,
    DESTROYER=10,
    MYSTERYSHIP=11,
}


