using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Shootable 
{
    void Move(GameObject obj, float speed);
    void OnCollide(GameObject obj);
}
