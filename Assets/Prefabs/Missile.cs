using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : Destroyer
{
    public float speed;


    private void Update() => base.Move(this.gameObject, speed);


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == (int)Layers.BOUNDARY || other.gameObject.layer == (int)Layers.PLAYER 
            || other.gameObject.layer == (int)Layers.PLAYERWEAPON) 
        {
            Destroy(this.gameObject);
        }
        
    }
}
