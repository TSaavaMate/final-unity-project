using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walls : MonoBehaviour
{
    [SerializeField]
    public GameObject ship;
    public float rotationSpeed;


    void Update() => this.transform.RotateAround(ship.transform.position, Vector3.forward, rotationSpeed * Time.deltaTime);
}
