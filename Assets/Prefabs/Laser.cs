using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : Weapon
{
    public float speed;


    void Update()
    {
        base.Move(this.gameObject, speed);
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        base.OnCollide(this.gameObject);
    }
}
