using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    int life;

    public Sprite[] sprites = new Sprite[2];
    SpriteRenderer renderer;
    public System.Action health;


    void Awake() => renderer = GetComponent<SpriteRenderer>();


    void Start()
    {
        life = 3;
        health = () => DecreaseLife();
    }


    void DecreaseLife() => renderer.sprite = (--life == 2) ? sprites[0] : sprites[1];

}
