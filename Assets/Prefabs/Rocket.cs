using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : Weapon
{
    public float speed;
    public float speedOfRotation;
    

    private void Update()
    {
        Move(this.gameObject,speed);
    }

    public override void Move(GameObject obj, float speed)
    {
        transform.position += direction * speed * Time.deltaTime;

        if (Input.GetKey(KeyCode.A))
        {
            Rotate(speedOfRotation);
        }
        else if (Input.GetKey(KeyCode.D))
        { 
            Rotate(-speedOfRotation); 
        }

    }
    void Rotate(float rotationDegree)
    {
        direction = Quaternion.Euler(0f, 0f, rotationDegree * Time.deltaTime) * direction;

        this.transform.Rotate(0f, 0f, rotationDegree * Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D other) => base.OnCollide(this.gameObject);
}
