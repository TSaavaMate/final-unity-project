using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceShip : MonoBehaviour
{
    public Vector3 directionH { get; private set; } = Vector3.right;

    public float speed;


    void Update() => MoveHorizontally();



    void MoveHorizontally()
    {
        this.transform.position += directionH * speed * Time.deltaTime;
        Vector3 leftEdge = new Vector3(-4f,0f,0f);
        Vector3 rightEdge = new Vector3(4f, 0f, 0f);

        foreach (Transform wall in transform)
        {
            if (!wall.gameObject.activeInHierarchy) continue;

            if (directionH == Vector3.right && wall.position.x >= (rightEdge.x ))
            {
                AdvanceRowHorizontally();
                break;
            }
            else if (directionH == Vector3.left && wall.position.x <= (leftEdge.x))
            {
                AdvanceRowHorizontally();
                break;
            }
        }
    }
    void AdvanceRowHorizontally() => this.directionH = new Vector3(-directionH.x, 0f, 0f);

}
