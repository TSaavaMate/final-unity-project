using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MysteryShip : Destroyer
{
    public float speed;

    private bool Destination => (vertical) ? 
                            transform.position.x >= 4f || transform.position.x <= -4f :
                            transform.position.y < (Mathf.Abs(transform.position.x) * (-3f / 4) + 1);

    bool vertical;

    public void SetOrder(bool order) => vertical = order;


    private void Update()
    {
        if (Destination)
            this.SetDirection(new Vector3(0, 0, 0));
        else
            base.Move(this.gameObject, speed);
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == (int)Layers.PLAYERWEAPON)
        {
            base.OnCollide(this.gameObject);
        }
    }


}
