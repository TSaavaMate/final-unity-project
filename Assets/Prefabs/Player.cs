using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Player : MonoBehaviour
{
    public float speed = 5f;
    public Rocket rocketRef;
    public Laser laserRef;
    public Bullet bullet;
    public GameObject playerArea;
    private Weapons type;
    int weaponNumber;

    private bool laserActive;
    private bool rocketActive;

    public System.Action destroyed;

    void Start()
    {
        type = Weapons.LASER;
    }
    private void Update()
    {
        Move();

        if (Input.GetKeyDown(KeyCode.Q))
        {
            weaponNumber++;
            type = (Weapons)(weaponNumber%3);
        }
            

        if (Input.GetKeyDown(KeyCode.Space))
        {
            switch(type)
            {
                case Weapons.BULLET:
                    ShootBullet();
                    break;
                case Weapons.ROCKET:
                    ShootRocket();
                    break;
                case Weapons.LASER:
                    ShootLaser();
                    break;
                default:
                    break;
            }
        }
           
    }
    void ShootBullet() => Instantiate(bullet, transform.position, Quaternion.identity);

    void ShootLaser()
    {
        if (!laserActive)
        {
            Laser laser = Instantiate(this.laserRef, transform.position, Quaternion.identity);
            laserActive = true;
            laser.destroyed += LaserDestroyed;
        }
    }
    void ShootRocket()
    {
        if (!rocketActive)
        {
            Rocket rocket = Instantiate(this.rocketRef, transform.position, Quaternion.identity);
            rocketActive = true;
            rocket.destroyed += RocketDestroyed;
        }
    }

    void LaserDestroyed() => laserActive = false;

    void RocketDestroyed() => rocketActive = false;

    void Move()
    {
        float horizontalInput = 0f;
        float verticalInput = 0f;

        if (Input.GetKey(KeyCode.LeftArrow))
            horizontalInput = -1f;
        else if (Input.GetKey(KeyCode.RightArrow))
            horizontalInput = 1f;

        if (Input.GetKey(KeyCode.UpArrow))
            verticalInput = 1f;
        else if (Input.GetKey(KeyCode.DownArrow))
            verticalInput = -1f;

        Vector3 movement = new Vector3(horizontalInput, verticalInput, 0f) * speed * Time.deltaTime;
        Vector3 newPosition = transform.position + movement;

        if(AreaCalculator.IsInside(newPosition,playerArea))
        {
            transform.position = newPosition;
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer != (int) Layers.DESTROYER) return;

        if(destroyed!=null) destroyed.Invoke();
        
    }
}
