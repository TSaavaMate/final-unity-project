using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour
{
    
    public GameObject player;

    public GameObject destroyerRef;

    public GameObject mysteryShip;

    private MysteryShip mysteryShipObj;

    public float shootingDelay => 3f;

    public System.Action destroyed;

    

    public List<GameObject> ships = new List<GameObject>();

    void Start()
    {
        //Invoke(nameof(SpawnShipsVertically), 10f);
        SpawnShipsVertically();
    }
    
    void Update()
    {
        if(ships.Count == 0 && this.transform.position.x<=0.5f && this.transform.position.x >= -0.5f) {
            SpawnShipsPyramidly();
        }
    }
        

    void SpawnShipsPyramidly()
    {
        for (float x = 0; x >= -4; x -= 1f)
        {
            Fire(new Vector3(x-this.transform.position.x ,  F(x)-3.2f, 0f));
            Fire(new Vector3(-x-this.transform.position.x , F(x)-3.2f, 0f));
        }
        InvokeRepeating(nameof(ShootRandom), shootingDelay, shootingDelay);
    }



    void SpawnShipsVertically()
    {
        for (float i = -1.5f; i >= -4.5f; i -= 1.0f)
        {
            Fire(new Vector3(3.8f, i, 0f),true);
            Fire(new Vector3(-3.8f, i, 0f),true);
        }
        InvokeRepeating(nameof(ShootRandom), shootingDelay, shootingDelay);
    }
    

    void Fire(Vector3 direction,bool order=false)
    {
        GameObject mysteryShip = Instantiate(this.mysteryShip, this.transform.position, Quaternion.identity);
        mysteryShip.SetActive(true);

        mysteryShipObj = mysteryShip.GetComponent<MysteryShip>();

        mysteryShipObj.SetDirection(direction);
        mysteryShipObj.SetOrder(order);
        mysteryShipObj.SetParams(player, destroyerRef);
        mysteryShipObj.destroyed = () => ShipDestroyed(mysteryShip);

        ships.Add(mysteryShip);
    }

    void ShootRandom()
    {
        if (ships.Count == 0) return;

        int index = Random.Range(0, ships.Count);
        if (ships[index].activeSelf)
        {
            ships[index].GetComponent<MysteryShip>().Shoot();
            
        }
    }
    void ShipDestroyed(GameObject ship)
    {
        ships.Remove(ship);
        Destroy(ship.gameObject);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer != (int)Layers.PLAYERWEAPON) return;
        

        if(destroyed!=null) destroyed.Invoke(); 
        
    }

    float F(float x) => Mathf.Abs(x) * (-3f / 4) + 1f;

}
