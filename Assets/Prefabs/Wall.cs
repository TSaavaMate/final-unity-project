using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{
    public Sprite[] destroyedSprites;


    protected SpriteRenderer spriteRenderer;
    protected float life ;
    protected float fullLife=15f;


    protected void Awake()
    {
        life = fullLife;
        spriteRenderer = GetComponent<SpriteRenderer>();
        
    }

    protected void Update()
    {
        Sprite sprite = spriteRenderer.sprite;


        if (life != fullLife && life % 3 == 0 )
        {
            ReplaceSprite(sprite);
        }

    }


    protected void ReplaceSprite(Sprite sprite)
    {   
        int level =(int)((life-1)/3);

        if (sprite.name == destroyedSprites[level].name) return;

        spriteRenderer.sprite = destroyedSprites[level];
        

    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer != (int)Layers.PLAYERWEAPON) return;
        
        if (--life == 0)
        {
            this.gameObject.SetActive(false);
        }
    }

}
