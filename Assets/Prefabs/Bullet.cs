using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : Weapon
{
    
    public float speed;


    void Update() => base.Move(this.gameObject, speed);


    void OnTriggerEnter2D(Collider2D other) => base.OnCollide(this.gameObject);
}
