using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Base 
{
    void OnEnter();
    void OnUpdate();
    void OnLeave();
}
