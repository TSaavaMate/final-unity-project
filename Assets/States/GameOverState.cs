using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverState : StateManager
{
    public override void OnEnter() => base.SetAnnouncment("Game Ended , You Loose! \n press enter to start again");

}
