using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinState : StateManager
{
    public override void OnEnter() => base.SetAnnouncment("Game Ended , You Won! \n  press enter to start again");
}
