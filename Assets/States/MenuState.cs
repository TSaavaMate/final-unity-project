using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuState : StateManager
{

    public override void OnEnter() => base.SetAnnouncment("      Welcome!\n press enter to play");


    public override void OnUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            StateMachine.instance.Change(GameManager.instance.states[States.PLAY]);
        }
    }

}

