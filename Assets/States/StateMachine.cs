using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
    public static StateMachine instance;

    public Base current;


    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public void Change(Base newState)
    {
        if (current != null)
            current.OnLeave();

        current = newState;

        current.OnEnter();
    }

    public void OnTick()
    {
        if (current != null)
        {
            current.OnUpdate();
        }   
    }
}
