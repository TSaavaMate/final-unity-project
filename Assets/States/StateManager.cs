using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public abstract class StateManager : MonoBehaviour, Base
{
    [Header("UI Text")]
    [SerializeField]
    TextMeshProUGUI announcment;

    [Header("Characters")]
    [SerializeField]
    protected GameObject[] gameObjects;


    public virtual void OnEnter() => SetAnnouncment();


    protected void SetAnnouncment(string announce = "") => announcment.SetText(announce);


    public virtual void OnUpdate() {}


    public virtual void OnLeave() => SetAnnouncment();

}
