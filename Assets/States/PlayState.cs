using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayState : StateManager
{
    bool isPlayerAlive;
    bool isEnemyAlive;

    int playerLife = 3;
    int enemyLife = 15;

    Health healthBar;


    List<GameObject> entities=new List<GameObject> ();
    Dictionary<Character, GameObject> characters = new Dictionary<Character, GameObject>();

    void Awake()
    {
        isPlayerAlive = true;
        isEnemyAlive = true;
    }
    
    
    public override void OnEnter()
    {
        base.OnEnter();
        SpawnEntities();
    }

    public override void OnUpdate()
    {
        if (isPlayerAlive && isEnemyAlive) return;
        
        foreach (var mysteryShip in characters[Character.ENEMY].GetComponent<Ship>().ships)
        {
            if (mysteryShip.activeSelf) entities.Add(mysteryShip);
            
        }
        
        
        if (!isEnemyAlive)
        {
            StateMachine.instance.Change(GameManager.instance.states[States.WIN]);
        }

        if (!isPlayerAlive)
        {
            StateMachine.instance.Change(GameManager.instance.states[States.GAMEOVER]);
        } 

    }
    public override void OnLeave() => DestroyEntities();



    void PlayerDestroyed()
    {
        if(--playerLife==0) isPlayerAlive = false;
        healthBar.health.Invoke();
    }
    void EnemyDestroyed()
    {
        if(--enemyLife==0) isEnemyAlive = false;
    }

    void SpawnEntities()
    {
        GameObject player=Instantiate(gameObjects[0], gameObjects[0].transform.position, Quaternion.identity);
        GameObject spaceShip = Instantiate(gameObjects[1], gameObjects[1].transform.position, Quaternion.identity);
        GameObject planet = Instantiate(gameObjects[2], gameObjects[2].transform.position, Quaternion.identity);
        GameObject health = Instantiate(gameObjects[3], gameObjects[3].transform.position, Quaternion.identity);
        GameObject boundaries = Instantiate(gameObjects[4], gameObjects[4].transform.position, Quaternion.identity);

        this.healthBar = health.gameObject.GetComponent<Health>();

        entities.Add(player);
        entities.Add(spaceShip);
        entities.Add(planet);
        entities.Add(health);
        entities.Add(boundaries);

        player.SetActive(true);
        spaceShip.SetActive(true);
        planet.SetActive(true);
        health.SetActive(true);


        characters.Add(Character.PLAYER, gameObjects[0]);
        characters.Add(Character.ENEMY, spaceShip.transform.GetChild(0).gameObject);

        spaceShip.transform.GetChild(1).gameObject.GetComponent<Walls>().ship = spaceShip.transform.GetChild(0).gameObject;
        spaceShip.transform.GetChild(0).gameObject.GetComponent<Ship>().player = player;


        player.GetComponent<Player>().destroyed = () => PlayerDestroyed();
        spaceShip.transform.GetChild(0).gameObject.GetComponent<Ship>().destroyed = () => EnemyDestroyed();

    }

    void DestroyEntities()
    {

        foreach (var entity in entities)
        {
            entity.SetActive(false);
        }
    }

}
